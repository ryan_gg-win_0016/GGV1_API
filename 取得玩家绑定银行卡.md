# /USER/addUserProxyPromo

取得玩家目前綁定銀行卡的資訊.

## Endpoint definition

`/USER/addUserProxyPromo`

## HTTP method

<span class="label label-primary">GET</span>

## Parameters

| Parameter | Description | Data Type |
|-----------|------|-----|


## Sample request

```
curl -I -X GET "http://kaijiang.goodgaming.com/index.php?s=/USER/addUserProxyPromo"
```

## Sample response

```json
{
	"status_code": 1,
	"$bank_account_list": [
		{
			"id": "120",
			"user_id": "373",
			"bank_account": "1234567899999997",
			"bank_owner": "anakin",
			"bank_list_id": "1",
			"bank_branch": "bank_branch",
			"bank_status": "1",
			"create_time": "2018-07-06 14:29:04",
			"first_id": "118",
			"user_account": "anakin32",
			"is_active": "1",
			"bank_name": "中国农业银行"
		},
		{
			"id": "119",
			"user_id": "373",
			"bank_account": "1234567899999998",
			"bank_owner": "anakin",
			"bank_list_id": "1",
			"bank_branch": "bank_branch",
			"bank_status": "1",
			"create_time": "2018-07-06 14:26:56",
			"first_id": "118",
			"user_account": "anakin32",
			"is_active": "1",
			"bank_name": "中国农业银行"
		},
		{
			"id": "118",
			"user_id": "373",
			"bank_account": "1234567899999999",
			"bank_owner": "anakin",
			"bank_list_id": "1",
			"bank_branch": "bank_branch",
			"bank_status": "1",
			"create_time": "2018-07-06 13:18:13",
			"first_id": null,
			"user_account": "anakin32",
			"is_active": "1",
			"bank_name": "中国农业银行"
		}
	]
}
```

## The following table describes each item in the response.

|Response item | Description |
|----------|------------|
| **status_code** | status_code. |
| **{bank_account_list}** | 玩家綁定銀行列表. |
| **{bank_account_list}\id** | 用戶銀行id. |
| **{bank_account_list}\user_id** | 玩家id. |
| **{bank_account_list}\bank_account** | 用戶銀行卡號. |
| **{bank_account_list}\bank_owner** | 用戶銀行卡戶名. |
| **{bank_account_list}\bank_list_id** | 銀行卡所屬銀行id. |
| **{bank_account_list}\bank_branch** | 銀行卡所屬分行. |
| **{bank_account_list}\create_time** | 創立時間. |
| **{bank_account_list}\first_id** | 第一張銀行卡. |
| **{bank_account_list}\user_account** | 用戶帳號. |
| **{bank_account_list}\is_active** | 用戶帳號是否啟動. |
| **{bank_account_list}\bank_name** | 銀行名稱. |



## Error and status codes

The following table lists the status and error codes related to this request.

| Status code | Meaning |
|--------|----------|
| 139 | Successful 用户在线资料错误 |



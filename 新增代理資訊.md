# /TEAM/userProxyAdd

新增代理返水资讯.

## Endpoint definition

`/TEAM/userProxyAdd`

## HTTP method

<span class="label label-primary">GET</span>

## Parameters

| Parameter | Description | Data Type |
|-----------|------|-----|

## Sample request

```
curl -I -X GET "http://ggtestapi.goodgaming.com//TEAM/teamReport"
```

## Sample response

```json
{
	"status_code": 1,
	"produxt_list": [
		{
			"id": "1",
			"product_name": "Lottery",
			"display_name": "彩票娱乐场",
			"user_prefix": null,
			"app_id": "87911298dc3eea962310c73dc23bbe53",
			"exchange_rate": "1.00000",
			"product_image": "__TMPL__Comm/Image/transfer_icon01.png",
			"product_order": "1",
			"percent_setting": "{\"percent_system\":\"15\",\"percent_max\":\"14\",\"percent_min\":\"0\",\"percent_range\":0.05,\"percent_quota_min\":\"13.1\",\"dailypay_max\":\"10\",\"dailypay_min\":\"0\"}",
			"product_setting": null,
			"is_show_index": "1",
			"is_active": "1",
			"is_balance": "1",
			"pn": "lottery",
			"name": "彩票娱乐场",
			"percent_name": "彩票返点",
			"range_list": [
				{
					"display": "1,980 - 14%",
					"value": 14
				},
				{
					"display": "1,979 - 13.95%",
					"value": 13.95
				},
				{
					"display": "1,978 - 13.9%",
					"value": 13.9
				},
			]
		},
		{
			"id": "2",
			"product_name": "Ag",
			"display_name": "AG",
			"user_prefix": "DM_",
			"app_id": "2bc8583f93100c4eb87a569c871b038b",
			"exchange_rate": "1.00000",
			"product_image": "__TMPL__Comm/Image/transfer_icon04.png",
			"product_order": "2",
			"percent_setting": "{\"percent_system\":\"15\",\"percent_max\":\"1.2\",\"percent_min\":\"0\",\"percent_range\":0.1,\"percent_quota_min\":\"1.2\"}",
			"product_setting": null,
			"is_show_index": "0",
			"is_active": "1",
			"is_balance": "0",
			"pn": "ag",
			"name": "AG",
			"percent_name": "AG返水",
			"range_list": [
				{
					"display": "1.2%",
					"value": 1.2
				},
				{
					"display": "1.1%",
					"value": 1.1
				},
				{
					"display": "1%",
					"value": 1
				},
			]
		},
	],
	"my_percent": "15",
	"max_percent": 1.2,
	"min_percent": 0,
	"percent_range": {
		"lottery": [
			{
				"display": "1,980 - 14%",
				"value": 14
			},
			{
				"display": "1,979 - 13.95%",
				"value": 13.95
			},
			{
				"display": "1,978 - 13.9%",
				"value": 13.9
			},
			{
				"display": "1,977 - 13.85%",
				"value": 13.85
			},
			{
				"display": "1,976 - 13.8%",
				"value": 13.8
			},
			{
				"display": "1,975 - 13.75%",
				"value": 13.75
			},
			{
				"display": "1,974 - 13.7%",
				"value": 13.7
			},
			{
				"display": "1,973 - 13.65%",
				"value": 13.65
			},
		],
		"ag": [
			{
				"display": "1.2%",
				"value": 1.2
			},
			{
				"display": "1.1%",
				"value": 1.1
			},
		],
	},
	"crumbName": "新增代理"
}

```

## The following table describes each item in the response.

|Response item | Description |
|----------|------------|
| **status_code** | status codes. |
| **{produxt_list}** | 產品LIST. |
| **{produxt_list}/product_name** | 產品名稱. |
| **{produxt_list}/display_name** | 顯示名稱. |
| **{produxt_list}/user_prefix** | 用戶前綴. |
| **{produxt_list}/app_id** | . |
| **{produxt_list}/exchange_rate** | 匯率. |
| **{produxt_list}/product_image** | 遊戲圖片. |
| **{produxt_list}/product_order** | 產品排序. |
| **{produxt_list}/percent_setting** | 返點設定. |
| **{produxt_list}/product_setting** | 產品設置. |
| **{produxt_list}/is_show_index** | 是否顯示在主列表. |
| **{produxt_list}/is_active** | 是否啟用0:否 1:是. |
| **{produxt_list}/is_balance** | 是否自己錢包0:沒有 1:有. |
| **{produxt_list}/pn** | 產品名稱. |
| **{produxt_list}/name** | 顯示名稱. |
| **{produxt_list}/percent_name** | . |
| **{produxt_list}/range_list** | range list. |
| **my_percent** | . |
| **max_percent** | . |
| **min_percent** | . |
| **{percent_range}** | . |
| **crumbName** | . |







## Error and status codes

The following table lists the status and error codes related to this request.

| Status code | Meaning |
|--------|----------|
| 139 | 用户在线资料错误 |
| 741 | 取得用户代理资料失败 |
| 742 | 无返水设定 |



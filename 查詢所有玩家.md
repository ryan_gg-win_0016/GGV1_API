# /USER

查詢所有玩家.

## Endpoint definition

`/USER`

## HTTP method

<span class="label label-primary">GET</span>

## Parameters

| Parameter | Description | Data Type |
|-----------|------|-----|


## Sample request

```
curl -I -X GET "http://ggtestapi.goodgaming.com/USER/id/all"
```

## Sample response

```json
{
	"259": "yummy100",
	"284": "09521984",
	"status_code": 1
}
```

## The following table describes each item in the response.

|Response item | Description |
|----------|------------|
| **key** | 用戶ID. |
| **value** | 帳號. |
| **status_code** | status codes. |

## Error and status codes

The following table lists the status and error codes related to this request.

| Status code | Meaning |
|--------|----------|
| 1 | Successful response |
| 115 | 请求参数错误 |
| 116 | 用户不存在 |



